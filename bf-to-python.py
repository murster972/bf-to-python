import argparse
import re


class _HelperFunctions:
    ''' defines the source code (as a list of lines) for helper functions used throuhgout
        the converted bf->python code '''
    def input(self):
        ''' get one-byte, convert to char-code, store in current cell '''
        return ["for _ in range(count):",
                f"\t{self._cur_cell()} = ord(sys.stdin.read(1))"]

    def output(self):
        ''' print out the current cell as ascii '''
        return ["for _ in range(count):",
                f"\tprint(chr({self._cur_cell()}), end='')"]  

    def left(self):
        ''' move cell left one place, wrap-around to end '''
        index = f"{BFToPython.INDEX_VAR}"
        return [f"global {index}",
                "for _ in range(count):",
                f"\t{index} -= 1 if {index} > {BFToPython.MIN_INDEX} else {BFToPython.MAX_INDEX}"]

    def right(self):
        ''' move cell right one place, wrap-around to start '''
        index = f"{BFToPython.INDEX_VAR}"
        return [f"global {index}",
                "for _ in range(count):",
                f"\t{index} += 1 if {index} < {BFToPython.MAX_INDEX} else {BFToPython.MIN_INDEX}"]

    def incr(self):
        ''' incr cur-cell value by one, wrap-around to 0 '''
        return ["for _ in range(count):",
                f"\t{self._cur_cell()} = {self._cur_cell()} + 1 if {self._cur_cell()} < {BFToPython.MAX_VALUE} else {BFToPython.MIN_VALUE}"]

    def decr(self):
        ''' decr cur-cell value by one, wrap-around to 255 '''
        return ["for _ in range(count):",
                f"\t{self._cur_cell()} = {self._cur_cell()} - 1 if {self._cur_cell()} > {BFToPython.MIN_VALUE} else {BFToPython.MAX_VALUE}"]

    def _cur_cell(self):
        ''' returns string refering to the current cell '''
        return f"{BFToPython.CELLS_VAR}[{BFToPython.INDEX_VAR}]"


class BFToPython:
    # regex to find all valid chars
    VALID_CHARS = re.compile(r"[<>+-.,\[\]]")

    OPENING_BRACKET = "["
    CLOSING_BRACKET = "]"

    # nmes of vars used in python source-code
    CELLS_VAR = "CELLS"
    INDEX_VAR = "CUR_INDEX"
    MIN_INDEX = "MIN_INDEX"
    MAX_INDEX = "MAX_INDEX"
    MIN_VALUE = "MIN_VALUE"
    MAX_VALUE = "MAX_VALUE"

    # init values of vars use in python source-code
    MIN_INDEX_VAL = 0
    MIN_VALUE_VAL = 0
    MAX_VALUE_VAL = 255

    def __init__(self):
        self._python_file = None
        self._bf_code = None

        self._ind_level = 0 

        # maps bf-characters to functions that will
        # insert approp code into python file
        #
        # NOTE: closing bracker "]" is not mapped since it will be
        #       handled in function for "]"
        self.char_map = {
            "+": lambda: "incr",
            "-": lambda: "decr",
            "<": lambda: "left",
            ">": lambda: "right",
            ",": lambda: "input",
            ".": lambda: "output"
        }

    def convert(self, bf_file, new_file=None, cell_size=100):
        ''' Reads through bf file and converts into python

            :param bf_file:   name of the bf file
            :param new_file:  name of the converted python file, will default to
                              the name of bf_file.py
            :param cell_size: number of cells in the bf program '''
        self._get_bf_code(bf_file)

        python_name = new_file if new_file else f"{bf_file}.py"

        # keeps track of the last char and the number of times it has occured
        # so that we can put repeated operations in loops
        #
        # NOTE: only non-loop chars
        last_char = ""
        count = 0

        with open(python_name, "w") as python_file:
            self._python_file = python_file

            self._write_vars(int(cell_size))
            self._write_common_functions()

            cur_char = True

            while cur_char:
                cur_char = self._bf_code[0] if self._bf_code else ""

                # write the previous character out to the python file
                # and reset counter and last-char tracker
                if cur_char != last_char:
                    if last_char in self.char_map:
                        self._non_loop(last_char, count)
                    count = 1
                    last_char = cur_char
                else:
                    count += 1
                
                if cur_char == self.OPENING_BRACKET:
                    self._open_loop()
                elif cur_char == self.CLOSING_BRACKET:
                    self._close_loop()
                
                if self._bf_code:
                    self._bf_code.pop(0)
                
    def _write(self, code):
        ''' writes python equivalent for current bf char out to code,
            while matching the current indent level '''
        self._python_file.write("{}{}\n".format('\t' * self._ind_level, code))

    def _non_loop(self, char, count):
        ''' add non-loop code into python source file '''
        funct_name = self.char_map[char]()

        if count == 1:
            self._write(f"{funct_name}()")
        else:
            self._write(f"{funct_name}({count})")

    def _open_loop(self):
        ''' returns the start of a new loop, checks that the new loop has a matching
            closing bracket and has some logical code (i.e. it's not empty and does
            not contain only loops) 
                    
            NOTE: should work as follows:
                - look for a closing bracket
                    - raise error if one does not exist
                - if one exists:
                    - write out a loop to the function
                    - increment the current tabs
                    - exit function
                        - the normal loop should work as normal then:
                            - for normal chars it will print inside the loop
                            - for nested loops it will repeat the above '''
        closing_ind = self._get_closing_bracket()

        if not closing_ind:
            raise Exception("unable to find closing bracket")
        
        self._write("while CELLS[CUR_INDEX] != 0:")
        self._ind_level += 1

        if self._is_loop_empty(self._bf_code[:closing_ind]):
            self._write("pass")

    def _close_loop(self):
        ''' checks if a closing bracket is valid and decrement current tab level '''
        if self._ind_level == 0:
            raise Exception("Invalid closing bracket - no matching opening bracket.")
        self._ind_level -= 1
        self._write("\n")

    def _get_closing_bracket(self):
        ''' returns ind of the closing bracket for current opening bracket, assumes
            the opening bracket is the first char in the code '''
        # increments when we encounter an opening bracket and decreases when
        # we see a closing bracket, 0 indicates that all opening have a closing
        # i.e. the current and all inner loops are closed
        #
        # starts at 1 since we're assuming that bf_code[0] is currently an 
        # opening bracket
        number_brackets = 1
        code_len = len(self._bf_code)

        for i in range(1, code_len + 1):
            if number_brackets == 0 or i == code_len:
                return None if number_brackets else i
            if self._bf_code[i] == self.OPENING_BRACKET:
                number_brackets += 1
            elif self._bf_code[i] == self.CLOSING_BRACKET:
                number_brackets -= 1

    def _is_loop_empty(self, loop):
        ''' checks if the passed loop is empty (i.e. no code or only loops) '''
        return len(re.findall("[^\[^\]]", "".join(loop))) == 0

    def _get_bf_code(self, bf_file_name):
        ''' reads code from bf-file, returns all valid bf-chars as a list (in-order they appear)

            :param bf_file_name: name of bf file '''
        with open(bf_file_name, "r") as f:
            self._bf_code = self.VALID_CHARS.findall(f.read())

    def _write_vars(self, cell_size):
        ''' writes vars used to the python file '''
        self._write(f"{self.CELLS_VAR} = [0] * {cell_size}\n"\
                    f"{self.INDEX_VAR} = 0\n"\
                    f"{self.MIN_INDEX} = {self.MIN_INDEX_VAL}\n"\
                    f"{self.MAX_INDEX} = {cell_size - 1}\n"\
                    f"{self.MIN_VALUE} = {self.MIN_VALUE_VAL}\n"\
                    f"{self.MAX_VALUE} = {self.MAX_VALUE_VAL}\n")

    def _write_common_functions(self):
        ''' writes following helper functions to the python file '''
        helpers = _HelperFunctions()

        self._write("import sys\n")

        for name, _ in vars(_HelperFunctions).items():
            if name.startswith("_"):
                continue
            
            body = "\n\t".join(getattr(helpers, name)())
            code = f"def {name}(count=1):\n\t{body}\n"
            self._write(code)

def main():
    parser = argparse.ArgumentParser(description="translate brain-fuck code into python.")
    parser.add_argument("bf_file", type=str, help="brain-fuck soure file")
    parser.add_argument("save_file", type=str, nargs="?", default=None,
                        help="name of the save-file, defaults to bf_file.py")

    args = parser.parse_args()
    
    b = BFToPython()
    b.convert(args.bf_file, args.save_file)

if __name__ == '__main__':
    main()