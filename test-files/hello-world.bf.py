CELLS = [0] * 100
CUR_INDEX = 0
MIN_INDEX = 0
MAX_INDEX = 99
MIN_VALUE = 0
MAX_VALUE = 255

import sys

def input(count=1):
	for _ in range(count):
		CELLS[CUR_INDEX] = ord(sys.stdin.read(1))

def output(count=1):
	for _ in range(count):
		print(chr(CELLS[CUR_INDEX]), end='')

def left(count=1):
	global CUR_INDEX
	for _ in range(count):
		CUR_INDEX -= 1 if CUR_INDEX > MIN_INDEX else MAX_INDEX

def right(count=1):
	global CUR_INDEX
	for _ in range(count):
		CUR_INDEX += 1 if CUR_INDEX < MAX_INDEX else MIN_INDEX

def incr(count=1):
	for _ in range(count):
		CELLS[CUR_INDEX] = CELLS[CUR_INDEX] + 1 if CELLS[CUR_INDEX] < MAX_VALUE else MIN_VALUE

def decr(count=1):
	for _ in range(count):
		CELLS[CUR_INDEX] = CELLS[CUR_INDEX] - 1 if CELLS[CUR_INDEX] > MIN_VALUE else MAX_VALUE

incr(8)
while CELLS[CUR_INDEX] != 0:
	right()
	incr(4)
	while CELLS[CUR_INDEX] != 0:
		right()
		incr(2)
		right()
		incr(3)
		right()
		incr(3)
		right()
		incr()
		left(4)
		decr()
	

	right()
	incr()
	right()
	incr()
	right()
	decr()
	right(2)
	incr()
	while CELLS[CUR_INDEX] != 0:
		left()
	

	left()
	decr()


right(2)
output()
right()
decr(3)
output()
incr(7)
output(2)
incr(3)
output()
right(2)
output()
left()
decr()
output()
left()
output()
incr(3)
output()
decr(6)
output()
decr(8)
output()
right(2)
incr()
output()
right()
incr(2)
output()
